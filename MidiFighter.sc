MidiFighter {
	classvar <>midiCh;
	classvar <>midiOut;
	classvar <>state; // MF's current state

	*new { |clock=false|
		MIDIIn.connectAll;
		if( MIDIClient.initialized.not ) { MIDIClient.init };
		// this.midiOut = MIDIOut.newByName("Midi Fighter Twister", "Midi Fighter Twister MIDI 1");
		this.midiOut = MIDIOut(0);
		this.midiOut.port.debug("Midi Fighter port");
		this.midiCh = IdentityDictionary().know_(true);
		this.midiCh.sys = 3;
		this.midiCh.ctl = 0;
		this.midiCh.btn = 1;
		this.midiCh.color = 1;
		this.midiCh.encAnim = 5;
		this.midiCh.btnAnim = 2;
		this.midiCh.shiftenc = 4;
		this.midiCh.sequencer = 7;
		this.state = IdentityDictionary().know_(true);
		this.state.knobs = Array.fill(127, 0);
		this.state.btns = Array.fill(127, 0);
		this.state.colors = Array.fill(127, 0);
		this.state.anims = Array.fill(127, 0);
		if(clock) {this.clock};
		super.new;
	}

	*clock { |tempoclock|
		tempoclock = tempoclock ?? TempoClock.default;
		Tdef(\midifighterClock, {
			inf.do{
				MIDIOut(0).midiClock;
				(1/240).wait;
			}
		}).play(tempoclock)
	}

	*cc { |num, func|
		MIDIdef((\cc++num).asSymbol, func);
	}

	*bank { |bank|
		this.midiOut.control(this.midiCh.sys, bank, 127);
	}

	/// \brief	Set an action when turning an encoder knob.
	///
	/// \param	num		Int			CC number of the knob.
	/// \param	func	Function	Takes one argument 'val', which is the
	/// 							incoming MIDI value that will be mapped
	/// 							by 'type' (see below).
	/// \param	type	Symbol		Determines how the incoming MIDI value
	/// 							will be mapped -- defaults to no mapping.
	///
	/// \usage	MidiFighter.kn(12, {|val| Ndef(\a).set(\amp, val)}, \norm)
	*kn { |num, func, type=\regular|
		var key = (\kn++num).asSymbol;
		var preFunc;
		var incrementFunc;
		if( type.asString.contains("bipolar") ) { this.setKn(num, 63) };

		// this function formats the original incoming midi value to
		// different ranges, depending on it's use.
		preFunc = {|val, midinum|
			var midival = val; // keep the original midi value
			switch (type,
				\regular, { },
				// increments or decrements depending on rotation direction
				\inc, {
					if( val > 63 ) {
						val = this.state.knobs[midinum] + (val-63);
					} {
						val = this.state.knobs[midinum] - (63-val);
					};

					// keep always in the middle -- similar to 'Enable Detent' in
					// MidiFighter's utility
					// this.prUpdateKn(midinum, 63);
					this.setKn(num, 63);
				},
				\bipolar, {
					val = val - 63;
				},
				\bipolarNorm, {
					val = val.linlin(0,127, -1.0, 1.0);
				},
				\norm, {
					val = val.linlin(0,127, 0.0, 1.0);
				},
				\freq, {
					if( val > 63 ) {
						val = this.state.knobs[midinum] + (val-63);
					} {
						val = this.state.knobs[midinum] - (63-val);
					};

					val = val.max(1).min(15000).debug("minmax");
					// keep always in the middle -- similar to 'Enable Detent' in
					// MidiFighter's utility
					// this.prUpdateKn(midinum, 63);
					this.setKn(num, 63);
				},
			);

			// FIX: ?? store the original midi value or the mapped one?
			// this.state.knobs[midinum] = midival;
			this.state.knobs[midinum] = val;
			val;
		};

		MIDIdef.cc(
			key: key,
			// 'func' is the function that the user defines. It takes
			// one argument 'val', which is provided by 'preFunc' above
			//  -- se function composition in SCHelp
			func: 	func <> preFunc,
			msgNum: num,
			chan: 	this.midiCh.ctl
		);

	}

	/// \brief	See '*.kn' above
	*btn { |num, func, type=\push|
		var key = (\btn++num).asSymbol;

		MIDIdef.cc(key, func <> {|val, midinum|
			if( type == \toggle && val != 0 ) {
				this.state.btns[num] = this.state.btns[num].asBoolean.not.asInteger * 127;
			};
			this.setBtn(num, this.state.btns[num]);
			this.state.btns[num].debug(num);
		}, num, this.midiCh.btn);

	}

	*setKn { |num, val|
		val.debug("set kn %".format(num));
		this.midiOut.control(this.midiCh.ctl, num, val);
	}

	*setBtn { |num, val|
		val.debug("set btn %".format(num));
		this.midiOut.control(this.midiCh.btn, num, val);
	}


	*setColor { |num, val|
		this.midiOut.control(this.midiCh.color, num, val);
	}

	*strobe { |num, val|
		this.midiOut.control(this.midiCh.btnAnim, num, val.mod(8));
	}

	*pulse { |num, val|
		this.midiOut.control(this.midiCh.btnAnim, num, val.mod(8) + 8);
	}

	*brightness { |num, val|
		this.midiOut.control(this.midiCh.btnAnim, num, val.mod(30) + 16);
	}

	*indicatorStrobe { |num, val|
		this.midiOut.control(this.midiCh.btnAnim, num, val.mod(8) + 48);
	}

	*indicatorPulse { |num, val|
		this.midiOut.control(this.midiCh.btnAnim, num, val.mod(8) + 56);
	}

	*indicatorBrightness { |num, val|
		this.midiOut.control(this.midiCh.btnAnim, num, val.mod(30) + 64);
	}

	*rainbow { |num, val|
		this.midiOut.control(this.midiCh.btnAnim, num, 127);
	}
}
